#!/usr/bin/env python3

import argparse, collections, datetime, functools, glob, io, logging, operator as op, os, sys, traceback, zipfile
import numpy as np, openpyxl, pandas as pd, pyexcelerate


EPS = 1.0e-10
MZ_TOL = 0.4
M = 6
R = 0.0107



# Media_13C full sets
params = {
	'lactic acid': [219, 203],
	'alanine': [190, 218],
	'glycerol': [218, 205, 191],
	'succinate': [247, 218, 172],
	'malate': [245, 233],
	'glutamate': [246, 258, 348],
	'citrate': [273]
}

# Lengyel_deconvolutions_for Sajjan app
params = {
	'glutamate': [147, 155, 203, 218, 230, 246],
	'lactate': [147, 174, 190, 203, 219]
}

# Renuka_test 3 for Sajjan_08142014_flux
params = {
	'lactate': [231, 261, 189, 159]
}

# Test Trial_LC_for sajjan.csv
params = {'sah': [385]}

# dHCY trials
params = {
	'cystathionine': [510, 292, 278, 245],
	'homocysteine': [234, 351],
	'methionine': [176, 293]
}

# Renuka_trial 2_ID103, ID118, ID119
params = {
	'lactic acid': [233, 261, 189, 159],
	'pyruvate': [174]
}

# 13c alanine_MTBSTFA_82814
params = {
	'alanine': [260, 232]
}

# Example_1_summer course
params = {
	'lactate': (5, 0.0107, [233, 261]),
	'alanine': (5, 0.0107, [260, 232])
}
params = {
	'lactic acid': (5, 0.0107, [233, 261]),
}

# homocysteine
params = {
	'homocysteine': (6, 0.000115, [420])
}

# Lengyel_adipocytes_TCA intermediates
params = {
	'pyruvate': (5, 0.0107, [174]),
	'lactate': (6, 0.0107, [233, 261]),
	'alanine': (6, 0.0107, [260, 232]),
	'serine': (4, 0.0107, [390]),
}



#
# Worksheet processing methods
#

def get_col(n):
	return openpyxl.cell.get_column_letter(n)


def create_new_sheet(wb, name, header = None):
	if header is None:
		return wb.new_sheet(name)

	else:
		ws = wb.new_sheet(name, data = [header])

		# Style header columns
		ws.range('A1', '%s1' % get_col(len(header))).style.alignment.horizontal = 'center'

		return ws


def resize_columns(ws, cols):
	for i, col in enumerate(cols):
		size = 10 if col == 'm/z' else max(20, len(col))
		ws.set_col_style(i + 1, pyexcelerate.Style(size = size))


def write_comments(ws, comments, n, offset):
	for i, s in enumerate(comments):
		ws.set_cell_value(n + i, 1 + offset, s)
		ws.get_cell_style(n + i, 1 + offset).font.bold = True
		ws.get_cell_style(n + i, 1 + offset).alignment.horizontal = 'left'



#
# Workbook creation class
#

class MIDProcessor:
	#
	# Magic Methods
	# 

	def __init__(self, filename, params):
		self.filename = filename
		self.basename = filename.rstrip('.csv')
		self.params = params

		self.error = None
		self.queued = True
		self.running = False
		self.runtime = -1

		# Initialize logging
		self._logger = logging.getLogger('')
		self._logger.setLevel(logging.INFO)
		self._log = io.StringIO()

		self._log_handler = logging.StreamHandler(self._log)
		self._log_handler.setLevel(logging.INFO)
		self._log_handler.setFormatter(logging.Formatter('%(asctime)s - %(message)s'))
		self._logger.addHandler(self._log_handler)

	def __del__(self):
		self._log.close()
		self._logger.removeHandler(self._log_handler)



	#
	# Logging methods
	#

	def log(self, msg):
		self._logger.info(msg)

	def debug(self, msg):
		self._logger.info(msg)



	#
	# Runtime methods
	#

	def get_log(self):
		return self._log.getvalue()

	def get_error(self):
		return self.error


	def is_queued(self):
		return self.queued

	def is_running(self):
		return self.running

	def is_error(self):
		return self.error is not None

	def is_finished(self):
		return not self.queued and not self.running and self.error is None

	def get_status(self):
		if self.is_queued():
			return 'Queued'
		elif self.is_running():
			return 'Running'
		elif self.is_error():
			return 'Error'
		elif self.is_finished():
			return 'Finished'
		else:
			return 'Unknown'


	def start(self):
		self.log('Starting processing task...')

		self.queued = False
		self.running = True

		self.start_time = datetime.datetime.now()

	def stop(self):
		self.log('Done!')

		self.running = False
		self.runtime = datetime.datetime.now() - self.start_time

	def load(self):
		self.queued = False
		self.running = False

		with open(self.basename +'.log') as f:
			self.log(f.read())




	#
	# Parsing methods
	#

	def parse_data(self):
		# Read data
		self.log('Reading %s...' % self.filename.split('/')[-1])
		self.df = pd.read_csv(self.filename, quotechar = '"', skipinitialspace = True)
		

		# Set up data frames
		self.log('Parsing data...')

		self.data = {}

		for k, (M, R, v) in self.params.items():
			self.data[k] = {m : pd.DataFrame(index = list(range(m, m + M + 1))) for m in v}

		# Set up column lists
		self.columns = {k : [] for k in self.params}

		# Set up workbooks
		self.wb = {k : pyexcelerate.Workbook() for k in self.params}
		self.wb['master'] = pyexcelerate.Workbook()


		# Parse data
		for i in range(0, len(self.df.columns), 2):
			# Get pair of columns and drop empty rows
			x = self.df.ix[:, i : i + 2]
			x.dropna(how = 'all', inplace = True)

			# Get sample information
			col = x.columns[1]
			compound = col.split('_')[0].lower()

			# Skip if compound is not to be analyzed
			if compound not in self.params:
				self.log('\tSkipping %s (no parameters provided)' % col)
				continue

			# Add column to column list
			self.columns[compound].append(col)

			# Rename columns
			x.columns = ['mz', 'data']

			# Read intensity values and combine if needed
			M, R, v = self.params[compound]

			for m in v:
				intensities = [sum(x[abs(x.mz - i) <= MZ_TOL].data) for i in range(m, m + M + 1)]
				self.data[compound][m][col] = pd.Series(intensities, index = self.data[compound][m].index)



	#
	# Worksheet generation methods
	#

	def write_raw_data(self):
		self.log('Writing raw data...')

		# Output raw data
		for k in self.params:
			ws = create_new_sheet(self.wb[k], 'Raw Data')


			# Add comments
			comments = [
				'Naming Scheme: Metabolite of Interest_ID_labeled or unlabeled'
			]

			write_comments(ws, comments, 1, 0)


			# Set row/column counters
			n = len(comments) + 2
			n_col = 1


			# Center align data
			ws.range('A%d' % n, '%s%d' % (get_col(len(self.df.columns)), n + len(self.df) + 1)).style.alignment.horizontal = 'center'

			for i in range(0, len(self.df.columns), 2):
				# Get pair of columns and drop empty rows
				x = self.df.ix[:, i : i + 2]
				x.dropna(how = 'all', inplace = True)

				if x.columns[1].lower().startswith(k):
					ws.set_cell_value(n, n_col, x.columns[1])
					ws.set_cell_value(n + 1, n_col, 'm/z')
					ws.set_cell_value(n + 1, n_col + 1, 'Abundance')
					ws.set_col_style(n_col, pyexcelerate.Style(size = len(x.columns[1])))
					ws.set_col_style(n_col + 1, pyexcelerate.Style(size = 10))

					ws.range('%s%d' % (get_col(n_col), n + 2), '%s%d' % (get_col(n_col + 1), n + len(x) + 2)).value = x.values

					n_col += 2


	def compute_fragment_data(self):
		self.log('Cleaning data and formatting fragment ions...')

		# Remove empty rows
		for k, (M, R, v) in self.params.items():
			# Prepare worksheet
			cols = ['m/z'] + self.columns[k]
			max_col = get_col(len(cols))

			ws = create_new_sheet(self.wb[k], 'Fragment Data', cols)

			# Format columns
			ws.range('A2', '%s%d' % (max_col, 1 + sum(len(self.data[k][m]) for m in v) + len(v))).style.alignment.horizontal = 'center'
			ws.range('A2', '%s%d' % (max_col, 1 + sum(len(self.data[k][m]) for m in v) + len(v))).style.format.format = '0'

			# Process data
			n = 2

			for m in v:
				self.data[k][m] = self.data[k][m][(self.data[k][m].T != 0).any()]

				if self.data[k][m].empty:
					self.params[k][2].remove(m)
					self.log('\tEmpty data frame for %s m/z = %d (no ions found)' % (k, m))

				else:
					self.data[k][m] = self.data[k][m].reindex(list(range(min(self.data[k][m].index), max(self.data[k][m].index) + 1)))
					self.data[k][m] = self.data[k][m].fillna(0)

					# Export data
					df = self.data[k][m][self.data[k][m].index >= m].reset_index()
					df['index'] = df['index'].apply(lambda x: str(x) + ' (M%+d)' % (x - m))

					ws.range('A%d' % n, '%s%d' % (max_col, n + len(self.data[k][m]))).value = df.values
					n += len(df) + 1

			# Auto size columns
			resize_columns(ws, cols)

			# Add comments
			comments = [
				'Selection of Fragments / Intact Ions'
			]

			write_comments(ws, comments, n, 1)


	def perform_backfilling(self):
		self.log('Performing backfilling of labeled data...')

		# Fill in zero intensities in the labeled data in order to improve final MID values
		for k, (M, R, v) in self.params.items():
			# Prepare worksheet
			cols = ['m/z'] + self.columns[k]
			max_col = get_col(len(cols))

			labeled = [col for col in self.columns[k] if '_labeled' in col.lower()]
			unlabeled = [col for col in self.columns[k] if '_unlabeled' in col.lower()]

			ws = create_new_sheet(self.wb[k], 'Backfilled Data', cols)

			# Format columns
			ws.range('A2', '%s%d' % (max_col, 1 + sum(len(self.data[k][m]) for m in v) + len(v))).style.alignment.horizontal = 'center'
			ws.range('A2', '%s%d' % (max_col, 1 + sum(len(self.data[k][m]) for m in v) + len(v))).style.format.format = '0'

			# Process data
			n = 2

			for m in v:
				for col in labeled:
					y = self.data[k][m][col]

					# Perform backfilling if required
					if (np.trim_zeros(y) < EPS).any():
						y_backfill = y * 0

						for ul_col in unlabeled:
							y_backfill += (y[m] * (np.trim_zeros(y) < EPS) * self.data[k][m][ul_col] / self.data[k][m][ul_col][m]).fillna(0)

						self.data[k][m][col] += y_backfill.reindex(index = y.index) / len(unlabeled)

				# Export data
				df = self.data[k][m][self.data[k][m].index >= m].reset_index()
				df['index'] = df['index'].apply(lambda x: str(x) + ' (M%+d)' % (x - m))

				ws.range('A%d' % n, '%s%d' % (max_col, n + len(self.data[k][m]))).value = df.values
				n += len(df) + 1

		# Auto size columns
		resize_columns(ws, cols)



	def compute_scaled_data(self):
		self.log('Scaling fragment data...')

		# Scale each data frame to median of column sums
		for k, (M, R, v) in self.params.items():
			# Prepare worksheet
			cols = ['m/z'] + self.columns[k]
			max_col = get_col(len(cols))

			ws = create_new_sheet(self.wb[k], 'Scaled Data', cols)

			# Format columns
			ws.range('A2', '%s%d' % (max_col, 1 + sum(len(self.data[k][m]) for m in v) + len(v))).style.alignment.horizontal = 'center'
			ws.range('A2', '%s%d' % (max_col, 1 + sum(len(self.data[k][m]) for m in v) + len(v))).style.format.format = '0'

			# Process data
			n = 2

			for m in v:
				print(self.data[k][m].sum().median() / self.data[k][m].sum())
				self.data[k][m] = self.data[k][m].mul(self.data[k][m].sum().median() / self.data[k][m].sum())
				print(self.data[k][m])

				# Export data
				df = self.data[k][m][self.data[k][m].index >= m].reset_index()
				df['index'] = df['index'].apply(lambda x: str(x) + ' (M%+d)' % (x - m))

				ws.range('A%d' % n, '%s%d' % (max_col, n + len(self.data[k][m]))).value = df.values
				n += len(df) + 1


			# Auto size columns
			resize_columns(ws, cols)

			# Add comments
			comments = [
				'Normalization of Intensities',
				'Intensities are normalized by scaling values to the median sum mTIC (metabolite total ion count) for each respective fragment/molecular ion.'
			]

			write_comments(ws, comments, n, 1)


	def compute_natural_abundance(self):
		self.log('Computing natural abundances...')

		# Compute unlabeled natural abundance
		for k, (M, R, v) in self.params.items():
			unlabeled = [col for col in self.columns[k] if '_unlabeled' in col.lower()]
			cols = ['m/z'] + unlabeled

			ws = create_new_sheet(self.wb[k], 'Unlabeled Natural Abundance', cols)

			# Process data
			n = 2

			for m in v:
				df = pd.DataFrame(index = self.data[k][m].index)

				for col in unlabeled:
					df[col] = self.data[k][m][col] / self.data[k][m][col][m]

				# Export data
				df = df[df.index >= m].reset_index()
				df['index'] = df['index'].apply(lambda x: str(x) + ' (M%+d)' % (x - m))

				ws.range('A%d' % n, '%s%d' % (get_col(len(cols)), n + len(df))).style.alignment.horizontal = 'center'
				ws.range('A%d' % n, 'A%d' % (n + len(df))).style.format.format = '0'
				ws.range('B%d' % n, '%s%d' % (get_col(len(cols)), n + len(df))).style.format.format = '0.000'

				ws.range('A%d' % n, '%s%d' % (get_col(len(cols)), n + len(df))).value = df.values

				n += len(df) + 1

			# Auto size columns
			resize_columns(ws, cols)


	def compute_differentials(self):
		self.log('Computing differentials...')

		# Compute differentials
		for k, (M, R, v) in self.params.items():
			# Prepare worksheet
			labeled = [col for col in self.columns[k] if '_labeled' in col.lower()]
			unlabeled = [col for col in self.columns[k] if '_unlabeled' in col.lower()]

			prefixes = ['_'.join(col.split('_')[:2])[:-1] for col in self.columns[k] if '_labeled' in col.lower()]
			prefixes = list(collections.OrderedDict.fromkeys(prefixes))
			block_sizes = {}

			# Define columns
			cols = ['m/z']

			for prefix in prefixes:
				for col in [x for x in labeled if x.startswith(prefix)]:
					for ul_col in unlabeled:
						cols.append(col +' - '+ ul_col)

				cols.append('Mean +/- SD')
				cols.append('')


			ws = create_new_sheet(self.wb[k], 'Differentials', cols)

			# Process data
			n, n_col = 2, 2

			for m in v:
				# Storage data frame
				df = pd.DataFrame(index = self.data[k][m].index)

				for prefix in prefixes:
					col_count = 0

					for col in [x for x in labeled if x.startswith(prefix)]:
						# Compute differentials
						for ul_col in unlabeled:
							df[col +' - '+ ul_col] = self.data[k][m][ul_col] - self.data[k][m][col]
							col_count += 1

					# Compute statistics
					mean = df[[s for s in cols if s.startswith(prefix)]].mean(axis = 1).apply(lambda x: '%.0f' % x)
					std = df[[s for s in cols if s.startswith(prefix)]].std(axis = 1).apply(lambda x: '%.0f' % x)
					df[prefix +' Mean +/- SD'] = (mean +' +/- '+ std).apply(lambda x: 0 if 'nan' in x else x)
					df[prefix +''] = pd.Series('', index = df.index)

					block_sizes[prefix] = col_count
					n_col += col_count + 2

				# Export data
				df = df[df.index >= m].reset_index()
				df['index'] = df['index'].apply(lambda x: str(x) + ' (M%+d)' % (x - m))
				ws.range('A%d' % n, '%s%d' % (get_col(len(cols)), n + len(df))).value = df.values

				n += len(df) + 1

			# Auto size columns
			resize_columns(ws, cols)


			# Highlight blocks and center data columns
			ws.range('A2', 'A%d' % n).style.alignment.horizontal = 'center'

			n_col = 2

			for i, (prefix, size) in enumerate(block_sizes.items()):
				for j in range(n_col, n_col + size + 1):
					for k in range(1, n - 1):
						# Store cell value
						val = ws.get_cell_value(k, j)

						# Highlight every other block
						if i % 2 == 0:
							ws.get_cell_style(k, j).fill.background = pyexcelerate.Color(255, 255, 0)

						# Format decimal
						ws.get_cell_style(k, j).format.format = '0'

						# Horizontal alignment
						ws.get_cell_style(k, j).alignment.horizontal = 'center'

						# Restore cell value
						ws.set_cell_value(k, j, val if val is not None else '')

				n_col += 2 + size


			# Add comments
			comments = [
				'Satellite peaks are defined as the isotopic peaks corresponding to a particular fragment/molecular ion (i.e. M+1, M+2,...,M+n)',
				'Differentials are determined by subtracting the normalized intensities of each isotopic peak in the unlabeled experiment from the corresponding peak in the labeled experiment',
				'',
				'General rules which apply to enriched fragments/molecular ions:',
				'1) M+0 in the unlabeled fragment is always > than the M+0 in the labeled fragment/molecular ion',
				'2) the first enriched satellite peak in the labeled fragment is always > than the corresponding satellite peak in the unlabeled fragment',
				'3) each satellite peak that comes after the first enriched satellite peak in the enriched fragment/molecular ion is always > than the corresponding satellite peaks in the unlabeled fragment/molecular ion'
			]

			write_comments(ws, comments, n, 1)


	def compute_matrix_mid(self):
		self.log('Computing MID via matrix method...')

		# Perform matrix calculation
		for k, (M, R, v) in self.params.items():
			# Prepare worksheet
			labeled = [col for col in self.columns[k] if '_labeled' in col.lower()]
			unlabeled = [col for col in self.columns[k] if '_unlabeled' in col.lower()]

			prefixes = []

			for col in self.columns[k]:
				if '_labeled' in col.lower():
					prefix = '_'.join(col.split('_')[:2])
					prefixes.append(prefix if prefix[-1].isdigit() else prefix[:-1])

			prefixes = list(collections.OrderedDict.fromkeys(prefixes))
			block_sizes = {}

			# Define columns
			cols = ['m/z']

			for prefix in prefixes:
				for col in [x for x in labeled if x.startswith(prefix)]:
					for ul_col in unlabeled:
						cols.append(col +' - '+ ul_col)

				cols.append('Mean +/- SD')
				cols.append('')


			ws = create_new_sheet(self.wb[k], 'Matrix MID', cols)
			ws_master = create_new_sheet(self.wb['master'], k.title() +' Matrix MID', cols)

			# Process data
			n, n_col = 2, 2

			for m in v:
				# Storage data frame
				df = pd.DataFrame(index = self.data[k][m].index)

				# Compute matrices
				matrices = {}

				for ul_col in unlabeled:
					# Ignore if invalid
					if m not in df.index or self.data[k][m][ul_col][m] == 0:
						self.log('\tSkipping invalid unlabeled column for m = %d: %s' % (m, ul_col))
						matrices[ul_col] = None
						continue

					# Build matrix elements
					matrix = pd.DataFrame(index = list(range(m, max(df.index) + 1)))

					x = pd.Series(self.data[k][m][ul_col])
					x_corr = x.tolist()

					for i in range(1, len(x_corr)):
						x_corr[i] -= functools.reduce(op.mul, [x_corr[0]] + [R] * i, 1)

					x_corr = pd.Series(x_corr, index = x.index)


					for i in range(max(x.index) - m + 1):
						matrix['M+%d' % i] = x if i == 0 else x_corr
						x_corr.index += 1

					# Convert NaN's to zeros
					matrix.fillna(0, inplace = True)

					# Build matrix
					A = np.matrix(matrix)


					# Build final transformation
					A = np.linalg.inv(A.transpose() * A) * A.transpose()

					# Store matrix
					matrices[ul_col] = A


				for prefix in prefixes:
					col_count = 0

					for col in [x for x in labeled if x.startswith(prefix)]:
						y = self.data[k][m][col][self.data[k][m].index >= m]

						# Compute MID
						for ul_col in unlabeled:
							A = matrices[ul_col]

							if A is None:
								df[col +' - '+ ul_col] = pd.Series(-1, index = df.index)
							else:
								# Compute result
								x = np.array(np.dot(A, y)).flatten()

								# Normalize
								x /= sum(x)

								# Align and store in data frame
								df[col +' - '+ ul_col] = pd.Series(x, index = list(range(m, m + len(x))))
							
							col_count += 1

					# Compute statistics
					mean = df[[s for s in cols if s.startswith(prefix)]].mean(axis = 1).apply(lambda x: '%.3f' % x)
					std = df[[s for s in cols if s.startswith(prefix)]].std(axis = 1).apply(lambda x: '%.3f' % x)
					df[prefix +' Mean +/- SD'] = (mean +' +/- '+ std).apply(lambda x: 0 if 'nan' in x else x)
					df[prefix +''] = pd.Series('', index = df.index)

					block_sizes[prefix] = col_count
					n_col += col_count + 2


				# Replace NaN's with zeros
				df = df.fillna(0)

				# Export data
				df = df[df.index >= m].reset_index()
				df['index'] = df['index'].apply(lambda x: str(x) + ' (M%+d)' % (x - m))
				ws.range('A%d' % n, '%s%d' % (get_col(len(cols)), n + len(df))).value = df.values
				ws_master.range('A%d' % n, '%s%d' % (get_col(len(cols)), n + len(df))).value = df.values

				n += len(df) + 1

			# Auto size columns
			resize_columns(ws, cols)
			resize_columns(ws_master, cols)

			# Highlight blocks and center data columns
			ws.range('A2', 'A%d' % n).style.alignment.horizontal = 'center'
			ws_master.range('A2', 'A%d' % n).style.alignment.horizontal = 'center'

			n_col = 2

			for i, (prefix, size) in enumerate(block_sizes.items()):
				for j in range(n_col, n_col + size + 1):
					for k in range(1, n - 1):
						# Store cell value
						val = ws.get_cell_value(k, j)
						val = ws_master.get_cell_value(k, j)

						# Highlight every other block
						if i % 2 == 0:
							ws.get_cell_style(k, j).fill.background = pyexcelerate.Color(255, 255, 0)
							ws_master.get_cell_style(k, j).fill.background = pyexcelerate.Color(255, 255, 0)

						# Format decimal
						ws.get_cell_style(k, j).format.format = '0.000'
						ws_master.get_cell_style(k, j).format.format = '0.000'

						# Horizontal alignment
						ws.get_cell_style(k, j).alignment.horizontal = 'center'
						ws_master.get_cell_style(k, j).alignment.horizontal = 'center'

						# Restore cell value
						ws.set_cell_value(k, j, val if val is not None else '')
						ws_master.set_cell_value(k, j, val if val is not None else '')

				n_col += 2 + size


			# Add comments
			comments = [
				'Mass Isotopomer Distribution was determined through an In-House MID Calculation Software',
				'Calculation of MID was centered around the matrices employed by (Jennings ME, 2nd, Matthews DE: Determination of complex isotopomer patterns in isotopically labeled compounds by mass spectrometry. Analytical chemistry 2005;77:6435-6444)',
				'MID values: 1 = 100%'
			]

			write_comments(ws, comments, n, 1)
			write_comments(ws_master, comments, n, 1)



	#
	# Export methods
	#

	def export_workbooks(self):
		self.log('Exporting workbooks...')

		for k in self.wb:
			self.log('Exporting %s to xlsx...' % k)
			self.wb[k].save(self.basename +'_%s.xlsx' % k)


	def export_log(self):
		with open(self.basename +'.log', 'w') as f:
			print(self.get_log(), file = f)
		
		self.log('Wrote to %s.log' % self.basename)


	def archive_results(self):
		self.log('Archiving...')

		# Get files to archive
		files = glob.glob(self.basename +'*')

		# Create ZIP archive
		archive = zipfile.ZipFile(self.basename +'.zip', 'w', zipfile.ZIP_DEFLATED)

		for f in files:
			archive.write(f, arcname = f.split('/')[-1])

			if not (f.endswith('.log') or f.endswith('_master.xlsx')):
				os.remove(f)

		archive.close()



def validate_file(data):
	return True


def process_task(processor):
	try:
		# Start processing
		processor.start()
		processor.parse_data()

		# Generate worksheets
		processor.write_raw_data()
		processor.compute_fragment_data()
		processor.perform_backfilling()
		processor.compute_scaled_data()
		processor.compute_natural_abundance()
		processor.compute_differentials()
		processor.compute_matrix_mid()

		# Export workbooks
		processor.export_log()
		processor.export_workbooks()
		processor.archive_results()

		# Stop
		processor.stop()


	except Exception as e:
		processor.error = e
		processor.log('Exception when processing', processor.filename)
		processor.log(traceback.format_exc())


if __name__ == '__main__':
	# Get arguments from command line
	parser = argparse.ArgumentParser(description = '')
	parser.add_argument('filenames', nargs = '+')
	filenames = parser.parse_args().filenames

	pd.options.mode.chained_assignment = None


	for filename in filenames:
		if os.path.exists(filename):
			processor = MIDProcessor(filename, params)

			ch = logging.StreamHandler(sys.stdout)
			ch.setLevel(logging.INFO)
			ch.setFormatter(logging.Formatter('%(asctime)s - %(message)s'))
			processor._logger.addHandler(ch)

			process_task(processor)
		
		else:
			print(filename +' does not exist')
