#!/usr/bin/env python3

import queue, threading


class Worker(threading.Thread):
	"""Worker task for executing processing tasks"""
	
	def __init__(self):
		self.running = True
		self.queue = queue.Queue()
		
		threading.Thread.__init__(self)
	
	def add_task(self, task_function, task):
		self.queue.put((task_function, task))
	
	def run(self):
		while self.running:
			task_function, task = self.queue.get()
			task_function(task)

	def stop(self):
		this.running = False