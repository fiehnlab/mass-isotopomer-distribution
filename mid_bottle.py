#!/usr/bin/env python3

from bottle import route, response, request, static_file, run
import datetime, json, os, time
import mass_isotopomer_distribution as mid, worker


#
# Jobs control
#


jobs = {}
worker = worker.Worker()


#
# Routing methods
#

# @route('/jobs')
# def jobs_list():
#     return json.dumps([{'id': i, 'name': jobs[i].basename, 'status': jobs[i].get_status()} for i in sorted(jobs, reverse = True)])

@route('/jobs/<timestamp>')
def job_status(timestamp = None):
	print(timestamp)

	if timestamp is None or timestamp not in jobs:
		return {'status': 'invalid'}

	elif jobs[timestamp].is_queued():
		return {'status': 'queued', 'log': 'Waiting for processing slot to open...'}

	elif jobs[timestamp].is_running():
		return {'status': 'running', 'log': jobs[timestamp].get_log()}

	elif jobs[timestamp].is_error():
		return {'status': 'error', 'log': jobs[timestamp].get_error()}

	elif jobs[timestamp].is_finished():
		return {'status': 'success', 'log': jobs[timestamp].get_log()}

	else:
		return {'status': 'error', 'error': 'Unknown status'}

@route('/jobs/<timestamp>/download')
def job_status(timestamp = None):
	if timestamp is None or timestamp not in jobs:
		return 'Job does not exist'

	elif jobs[timestamp].is_queued() or jobs[timestamp].is_running():
		return 'Job is running'

	elif jobs[timestamp].is_error():
		return 'Job experienced a processing error'

	elif jobs[timestamp].is_finished():
		print(jobs[timestamp].basename +'.zip')
		return static_file(jobs[timestamp].basename +'.zip', root = '.', download = True)

	else:
		return 'Unknown status'



@route('/', method = 'GET')
def root():
	return static_file('index.html', root = 'static')

@route('<filename:path>', method = 'GET')
def static_content(filename):
	return static_file(filename, root = 'static')


@route('/', method = 'POST')
def process():
	for k in request.forms.keys():
		print(k, request.forms[k])

	# Get POST data
	parameters = request.forms.parameters
	csv_file = request.files.csv_file


	# Get directory names and filenames
	timestamp = datetime.datetime.now().strftime('%Y%m%d%H%M%S%f')
	dirname = 'data/%s/' % timestamp
	csv_filename = dirname + csv_file.filename
	basename = csv_filename.rstrip('.csv')


	# Validate CSV file
	data = csv_file.file.read()

	if not mid.validate_file(data):
		return {'status': 'error', 'error': 'invalid csv fil'}


	# Create new working directory and write CSV file
	os.makedirs(dirname)

	with open(csv_filename, 'wb') as f:
		f.write(data)


	# Process parameters
	compounds = {}

	for compound in json.loads(parameters):
		if 'mass' in compound and compound['mass'] is not None:
			# Extract name and mass list
			name = compound['name']
			masses = list(map(int, compound['mass'].split(',')))

			# Get correction factor
			if 'correction' in compound and compound['correction'] is not None:
				correction = float(compound['correction'])
			else:
				correction = 0.0107

			# Get mass range
			if 'range' in compound and compound['range'] is not None:
				mass_range = int(compound['range'])
			else:
				mass_range = 5

			# Store compound parameters
			compounds[name] = (mass_range, correction, masses)


	# Create processing task
	task = mid.MIDProcessor(csv_filename, compounds)

	# Add processing task to the queue and jobs list
	jobs[timestamp] = task
	worker.add_task(mid.process_task, task)


	# Return queued response code
	return {'status': 'queued', 'id': timestamp}



if __name__ == '__main__':
	# Get all existing jobs
	data = os.walk('data')
	next(data)

	for directory, _, files in data:
		job = directory.lstrip('data/')
		zip_files = [f for f in files if f.endswith('.zip')]
		log_files = [f for f in files if f.endswith('.log')]

		if len(zip_files) == 1 and len(log_files) == 1:
			basename = log_files[0].rstrip('.log')
			print(job, basename)

			jobs[job] = mid.MIDProcessor('data/'+ job +'/'+ basename, [])
			jobs[job].load()


	# Set worker thread as daemon thread and start
	worker.daemon = True
	worker.start()

	# Start http server
	run(host = '0.0.0.0', port = 8888)