angular.module('app', ['ngRoute', 'ui.bootstrap'])
    /**
     *
     */
    .config(function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'main.html',
                controller: 'MainController'
            })
            .when('/about', {
                templateUrl: 'about.html'
            })
            .when('/concentration', {
                templateUrl: 'concentration.html',
                controller: 'ConcentrationController'
            })
            .when('/jobs/:id', {
                templateUrl: 'job.html',
                controller: 'JobController'
            });
    })


    /**
     *
     */
    .controller('NavigationController',function($scope, $location) {
        $scope.navClass = function(page) {
            var currentRoute = $location.path().substring(1) || 'home';
            return page === currentRoute ? 'active' : '';
        };
    })


    /**
     *
     */
    .controller('MainController', function($scope, $http, $location, CacheService) {
        $scope.compounds = [];

        /**
         * Parse the header of the input csv file to extract compound names
         */
        $scope.validateFileUpload = function() {
            // Set up file reader
            var fileReader = new FileReader();

            fileReader.onload = function(e) {
                // Read header
                var data = e.target.result;
                var header = data.split('\n')[0].toLowerCase().split(',');

                // Parse compounds
                var compounds = {};

                for(var i = 1; i < header.length; i += 2)
                    compounds[header[i].split('_')[0]] = true;

                compounds = Object.keys(compounds);

                // Add compounds
                $scope.compounds = [];

                for(var i = 0; i < compounds.length; i++)
                    $scope.compounds.push({name: compounds[i]});

                $scope.$apply();
            };

            fileReader.readAsText($scope.file);
        };

        $scope.submitProcessingTask = function() {
            // Build form data
            var data = new FormData();
            data.append('csv_file', $scope.file);
            data.append('parameters', angular.toJson($scope.compounds));

            // POST data
            $http.post('/', data, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).success(function(data, status, headers, config) {
                console.log(data);

                // If data was processed successfully, redirect to download page
                if(data.status == 'queued') {
                    CacheService.setValue('job', data);
                    $location.path('jobs/'+ data.id);
                }

                // Otherwise, show form validation errors
                else {

                }
            }).error(function(data, status, headers, config) {
                console.log('Error')
                console.log(data);
            });
        };
    })


    /**
     *
     */
    .controller('JobController', function($scope, $routeParams, $timeout, $http, CacheService) {
        $scope.job = { status: 'queued' };

        $scope.refreshJobStatus = function() {
            $timeout(function() {
                $http.get('/jobs/'+ $routeParams.id)
                    .success(function(data, status, headers, config) {
                        $scope.job = data;
                        //$scope.$apply();

                        if($scope.job.status === 'queued' || $scope.job.status === 'running')
                            $scope.refreshJobStatus();
                        else if($scope.job.status === 'success' && $scope.auto_download)
                            window.location.href = $scope.download_url;
                    });
            }, 1000);
        };

        (function() {
            // Add data to scope
            $scope.job = CacheService.getValue('job');
            $scope.download_url = 'jobs/'+ $routeParams.id +'/download';

            // Set auto download if job is currently running
            if(angular.isDefined($scope.job) && ($scope.job.status === 'queued' || $scope.job.status === 'running'))
                $scope.auto_download = true;

            $scope.refreshJobStatus();
        })();
    })


    /**
     *
     */
    .controller('ConcentrationController', function($scope, XLSXReaderService) {
        $scope.data = {};
        $scope.maxLength = 0;

        $scope.validateFileUpload = function() {
            XLSXReaderService.readFile($scope.file).then(function(xlsxData) {
                $scope.data = {};
                $scope.maxLength = 0;
                $scope.range = function(n) { return new Array(n); };

                var sheets = xlsxData.sheets;

                for(var k in sheets) {
                    if(!sheets.hasOwnProperty(k) || k.indexOf(' Matrix MID') == -1)
                        continue;

                    $scope.data[k] = {};

                    // Get M+0 values
                    M = [];

                    for(var i = 0; i < sheets[k].data.length; i++) {
                        var row = sheets[k].data[i][0];

                        if(angular.isDefined(row) && row.indexOf('(M+0)') > -1)
                            M.push(parseInt(row.split(' ')[0]));
                    }

                    // Get IDs
                    var id = null;

                    for(var i = 0; i < sheets[k].data[0].length; i++) {
                        var header = sheets[k].data[0][i];

                        if(header.indexOf(' - ') > -1) {
                            id = header.split(' - ')[0].split('_')[1];

                            // Remove last character if it is not a number
                            if(isNaN(parseInt(id.slice(-1))))
                                id = id.slice(0, -1);
                        }

                        else if(header == 'Mean +/- SD') {
                            var blockCount = 0;
                            var j = 1;

                            while(blockCount < M.length) {
                                var mid = [];

                                while(angular.isDefined(sheets[k].data[j][i]) && sheets[k].data[j][i] != '') {
                                    mid.push(parseFloat(sheets[k].data[j][i]));
                                    j++;
                                }

                                $scope.data[k][id +' (M = '+ M[blockCount] +')'] = {'mid': mid};
                                $scope.maxLength = Math.max($scope.maxLength, mid.length);
                                blockCount++;
                                j++;
                            }
                        }
                    }

                    // Add concentration and update length of MID
                    for(var compound in $scope.data[k]) {
                        if(!$scope.data[k].hasOwnProperty(compound))
                            continue;

                        while($scope.data[k][compound].mid.length < $scope.maxLength)
                            $scope.data[k][compound].mid.push(0.0);

                        $scope.data[k][compound].concentration = 0.0;
                    }
                }
            });
        };
    })


    /**
     * Based on https://gist.github.com/psjinx/8252417
     */
    .factory("XLSXReaderService", function($q, $rootScope) {
        var service = function(data) {
            angular.extend(this, data);
        };

        service.readFile = function(file) {
            var deferred = $q.defer();

            XLSXReader(file, true, false, function(data) {
                $rootScope.$apply(function() {
                    deferred.resolve(data);
                });
            });

            return deferred.promise;
        };

        return service;
    })


    /**
     *
     */
    .service('CacheService', function() {
        var cache = {};

        return {
            getValue: function(key) {
                if(cache.hasOwnProperty(key)) {
                    var value = cache[key];
                    delete cache[key];
                    return value;
                }
            },
            setValue: function(key, value) {
                cache[key] = value;
            }
        };
    })


    /**
     *
     */
    .directive('ngFileSelect', function () {
        return {
            link: function($scope, el) {
                el.bind('change', function(e) {
                    $scope.file = (e.srcElement || e.target).files[0];
                    $scope.validateFileUpload();
                });
            }
        }
    })


    /**
     * Converts a string to title case (each word is capitalized)
     */
    .filter('titlecase', function() {
        return function(s) {
            s = ( s === undefined || s === null ) ? '' : s;

            return s.toString().toLowerCase().replace( /\b([a-z])/g, function(ch) {
                return ch.toUpperCase();
            });
        };
    });